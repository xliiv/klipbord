use crate::error;
use crate::Result;
use core::num::NonZeroU32;
use ring::aead::{Aad, BoundKey, Nonce, NONCE_LEN};
use ring::pbkdf2;
use ring::rand::{SecureRandom, SystemRandom};
use std::fs;
use std::path::PathBuf;

// aead is described in details here https://tools.ietf.org/html/rfc5116

static PBKDF2_ITERATIONS: u32 = 100;
static ITERATION_MSG: &'static str = "PBKDF2_ITERATIONS should be greater than 0";

struct OneNonceSequence(Option<ring::aead::Nonce>);

impl OneNonceSequence {
    fn new(nonce: ring::aead::Nonce) -> Self {
        Self(Some(nonce))
    }
}

impl ring::aead::NonceSequence for OneNonceSequence {
    fn advance(&mut self) -> std::result::Result<ring::aead::Nonce, ring::error::Unspecified> {
        self.0.take().ok_or(ring::error::Unspecified)
    }
}

fn get_random_nonce() -> std::result::Result<(Nonce, [u8; 12]), ring::error::Unspecified> {
    let rand_gen = SystemRandom::new();
    let mut raw_nonce = [0u8; NONCE_LEN];
    rand_gen.fill(&mut raw_nonce)?;
    Ok((Nonce::assume_unique_for_key(raw_nonce), raw_nonce))
}

fn get_unbound_key(
    passphrase: &str,
    iteration: NonZeroU32,
    salt: &[u8],
) -> std::result::Result<ring::aead::UnboundKey, ring::error::Unspecified> {
    let mut key = [0; 32];
    pbkdf2::derive(
        pbkdf2::PBKDF2_HMAC_SHA256,
        iteration,
        salt,
        passphrase.as_bytes(),
        &mut key,
    );
    ring::aead::UnboundKey::new(&ring::aead::AES_256_GCM, &key)
}

/// Generates salt.
///
/// Encryption salt contains
/// - system salt (other systems stay safe if one system is cracked)
/// - encryption salt (other encryptions are safe if one encryption is cracked)
///
/// See https://briansmith.org/rustdoc/ring/pbkdf2/index.html
fn get_salt(enc_salt: &str) -> Vec<u8> {
    let system_salt = [0, 1, 2, 3, 4, 5, 6, 7];
    let enc_bytes = enc_salt.as_bytes();
    let mut salt: Vec<u8> = Vec::with_capacity(system_salt.len() + enc_bytes.len());
    salt.extend(&system_salt);
    salt.extend(enc_bytes);
    salt
}

/// Encrypts `to_encrypt` text using `passphrase`.
pub fn encrypt(to_encrypt: String, passphrase: &str) -> Result<Vec<u8>> {
    let mut in_out = to_encrypt.as_bytes().to_vec();
    let iteration = NonZeroU32::new(PBKDF2_ITERATIONS).expect(ITERATION_MSG);
    // FIXME: we should pass in e.g. file name but encrypt fn wasn't desigend for it and it will be
    // redesigned
    let salt = get_salt("");
    let unbound_key =
        get_unbound_key(passphrase, iteration, &salt).map_err(|e| error::Error::Encrypt(e))?;
    let (nonce, raw_nonce) = get_random_nonce().map_err(|e| error::Error::Encrypt(e))?;
    let nonce_sequence = OneNonceSequence::new(nonce);
    let mut s_key: ring::aead::SealingKey<OneNonceSequence> =
        BoundKey::new(unbound_key, nonce_sequence);
    s_key
        .seal_in_place_append_tag(Aad::empty(), &mut in_out)
        .map_err(|e| error::Error::Encrypt(e))?;
    in_out.extend(&raw_nonce);
    Ok(in_out)
}

/// Decrypts `to_decrypt` bytes using `passphrase`.
pub fn decrypt(mut to_decrypt: &mut Vec<u8>, passphrase: &str) -> Result<String> {
    let iteration = NonZeroU32::new(PBKDF2_ITERATIONS).expect(ITERATION_MSG);
    // FIXME: we should pass in e.g. file name but decrypt fn wasn't desigend for it and it will be
    // redesigned
    let salt = get_salt("");
    let unbound_key =
        get_unbound_key(passphrase, iteration, &salt).map_err(|e| error::Error::Decrypt(e))?;
    let nonce = to_decrypt.split_off(to_decrypt.len() - NONCE_LEN);
    let nonce = Nonce::try_assume_unique_for_key(&nonce).map_err(|e| error::Error::Decrypt(e))?;
    let nonce_sequence = OneNonceSequence::new(nonce);
    let mut o_key: ring::aead::OpeningKey<OneNonceSequence> =
        BoundKey::new(unbound_key, nonce_sequence);
    let decrypted = o_key
        .open_in_place(Aad::empty(), &mut to_decrypt)
        .map_err(|e| error::Error::Decrypt(e))?;
    let text =
        std::string::String::from_utf8(decrypted.to_vec()).map_err(|e| error::Error::Decode(e))?;
    Ok(text)
}

/// Gets default path to file where user password is stored.
pub fn default_path() -> Option<PathBuf> {
    dirs::config_dir().and_then(|mut p| {
        p.push("klipbord");
        p.push("passphrase");
        Some(p)
    })
}

/// Read passphrase from file at `path`.
pub fn read_passphrase_path(path: &PathBuf) -> Result<String> {
    let passphrase =
        fs::read_to_string(&path).map_err(|e| error::Error::PassphraseRead(path.to_owned(), e))?;
    let trimmed = passphrase.trim_end_matches(|c| c == '\n' || c == '\r');
    Ok(String::from(trimmed))
}

/// Read passphrase at default path.
pub fn read_passphrase() -> Result<String> {
    read_passphrase_path(&default_path().ok_or(error::Error::Environment(
        "Can't get default encryption dir",
    ))?)
}
