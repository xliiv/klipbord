use structopt::StructOpt;

const ABOUT: &str = "
Usage:
    1. Run `klipbord --recording` to record the system clipboard content
    2. Run `klipbord --select` to select from the recorded items
";

#[derive(Debug, StructOpt)]
#[structopt(name = "klipbord", about = ABOUT)]
pub struct Cli {
    /// Runs a record loop which records the clipboard content.
    #[structopt(short = "r", long = "recording")]
    pub recording: bool,

    /// Runs a window where user can select from the recorded content.
    #[structopt(short = "s", long = "select")]
    pub select: bool,
}
