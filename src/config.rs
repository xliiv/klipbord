use serde::{Deserialize, Serialize};
use std::fs;
use std::io;
use std::path::{Path, PathBuf};
use toml::de;

use crate::error;
use crate::Result;

/// Stores configuration settings.
#[derive(Debug, Default, Deserialize, Serialize)]
pub struct Config {
    pub syncer_dir: PathBuf,
    pub encryption: bool,
    pub record_interval_ms: std::time::Duration,
}

impl Config {
    /// Creates Config reading a default file or with defaults.
    pub fn new() -> Config {
        Config::default()
    }

    /// Gets config from a file at `path`.
    ///
    /// If `path` doesn't exist default config is returned.
    pub fn read_or_create(path: &Path, config: Config) -> Result<Config> {
        match fs::read_to_string(path) {
            Err(e) if e.kind() == io::ErrorKind::NotFound => {
                config.to_toml(&path)?;
                Ok(config)
            }
            Err(e) => Err(error::Error::ConfigRead(path.into(), e)),
            Ok(content) => content
                .parse()
                .map_err(|e| error::Error::ConfigParse(path.into(), e)),
        }
    }

    /// Saves config in a file at `path`.
    pub fn to_toml(&self, path: &Path) -> Result<()> {
        let toml_content =
            toml::to_string(&self).map_err(|e| error::Error::ConfigSerialize(path.into(), e))?;
        fs::write(path, toml_content).map_err(|e| error::Error::ConfigWrite(path.into(), e))?;
        Ok(())
    }
}

impl std::str::FromStr for Config {
    type Err = de::Error;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        toml::from_str(s)
    }
}
