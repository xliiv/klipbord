use crate::widgets::Focus;
use crate::{FileChannel, Klipbord, SwapChannel};
use druid::lens::{self, LensExt};
use druid::widget::{Button, Flex, Label, List, Scroll, TextBox, WidgetExt};
use druid::{AppLauncher, Data, Lens, LocalizedString, Widget, WindowDesc};
use std::sync::Arc;

#[derive(Clone, Data, Lens, Debug)]
struct AppData {
    searched: String,
    found: Arc<Vec<String>>,
    klipbord: Arc<crate::storage::FileStorage>,
    channel: Arc<FileChannel>,
}

/// Show GUI where user can select from clipboard history.
pub fn show_gui(klipbord: crate::storage::FileStorage, channel: FileChannel) {
    dbg!("show_gui");

    let window = WindowDesc::new(build_widget).title(LocalizedString::new("Klipbord"));
    let data = AppData {
        searched: "".to_string(),
        found: Arc::new(Vec::new()),
        klipbord: Arc::new(klipbord),
        channel: Arc::new(channel),
    };
    dbg!(&data.klipbord);
    AppLauncher::with_window(window)
        .use_simple_logger()
        .launch(data)
        .expect("launch failed");

    dbg!("GUI killed");
}

fn build_widget() -> impl Widget<AppData> {
    let search_input = TextBox::raw().lens(lens::Id.map(
        |d: &AppData| d.searched.clone(),
        |d: &mut AppData, new_value: String| {
            d.searched = new_value;
            let history = d.klipbord.get_all().unwrap_or_else(|e| {
                eprintln!("{}", e);
                Vec::new()
            });
            d.found = Arc::new(
                history
                    .iter()
                    .filter(|i| {
                        if d.searched.as_ref() as &str == "" {
                            return true;
                        }
                        i.to_lowercase().contains(&d.searched.to_lowercase())
                    })
                    .map(|s| s.to_string())
                    .collect::<Vec<String>>(),
            );
        },
    ));
    let found_list = Scroll::new(List::new(|| {
        Flex::row()
            .with_child(
                Label::new(|(_, item): &(_, String), _env: &_| format!("{}", item)).padding(5.0),
                1.0,
            )
            .with_child(
                Button::sized(
                    "Copy",
                    |evt_ctx, (channel, selected): &mut (Arc<FileChannel>, String), _env| {
                        dbg!(&selected);
                        if let Err(e) = channel.send(selected) {
                            // TODO: show on GUI
                            eprintln!("{}", e);
                        }
                        evt_ctx.submit_command(druid::commands::CLOSE_WINDOW, None);
                    },
                    80.0,
                    20.0,
                )
                .padding(5.0),
                0.0,
            )
    }))
    .vertical()
    .lens(lens::Id.map(
        |d: &AppData| (d.channel.clone(), d.found.clone()),
        |_: &mut AppData, _: (Arc<FileChannel>, Arc<Vec<String>>)| {},
    ));

    Flex::column()
        .with_child(Focus::new(search_input).padding(5.0), 0.0)
        .with_child(found_list.padding(5.0), 1.0)
}
