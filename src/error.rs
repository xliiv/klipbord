use std::error;
use std::fmt;
use std::io;
use std::path::PathBuf;
use toml::de;
use toml::ser;

#[derive(Debug)]
pub enum Error {
    Conversion(std::string::FromUtf8Error),
    Environment(&'static str),
    IO(Option<PathBuf>, io::Error),
    ConfigParse(PathBuf, de::Error),
    ConfigRead(PathBuf, io::Error),
    ConfigSerialize(PathBuf, ser::Error),
    ConfigWrite(PathBuf, io::Error),
    Encrypt(ring::error::Unspecified),
    Decrypt(ring::error::Unspecified),
    Decode(std::string::FromUtf8Error),
    PassphraseRead(PathBuf, io::Error),
    Regex(String, regex::Error),
}

impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        None
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self {
            Error::Conversion(error) => write!(f, "Conversion error: {error}", error = error),
            Error::Environment(error) => write!(f, "Environement error: {error}", error = error),
            Error::IO(path, error) => write!(
                f,
                "IO error for path {path:?}: {error}",
                path = path.as_ref().and_then(|p| p.to_str()).unwrap_or("Unknown"),
                error = error
            ),
            Error::ConfigRead(path, error) => write!(
                f,
                "Error reading config {path:?}: {error}",
                path = path,
                error = error
            ),
            Error::ConfigParse(path, error) => write!(
                f,
                "Error parsing config {path:?}: {error}",
                path = path,
                error = error
            ),
            Error::ConfigWrite(path, error) => write!(
                f,
                "Error writing config {path:?}: {error}",
                path = path,
                error = error
            ),
            Error::ConfigSerialize(path, error) => write!(
                f,
                "Error serializing config {path:?}: {error}",
                path = path,
                error = error
            ),
            Error::Encrypt(error) => write!(f, "Error encrypting message: {error}", error = error),
            Error::Decrypt(error) => write!(f, "Error decrypting message: {error}", error = error),
            Error::Decode(error) => write!(f, "Error decoding message: {error}", error = error),
            Error::PassphraseRead(path, error) => write!(
                f,
                "Error reading passphrase {path:?}: {error}",
                path = path,
                error = error
            ),
            Error::Regex(pattern, error) => write!(
                f,
                "Error creating regex from {pattern:?}: {error}",
                pattern = pattern,
                error = error
            ),
        }
    }
}

impl std::convert::From<std::io::Error> for Error {
    fn from(error: io::Error) -> Self {
        Error::IO(None, error)
    }
}
