use crate::encryption;
use crate::error;
use crate::Klipbord;
use regex::Regex;
use std::fs;
use std::io::prelude::*;
use std::path::{Path, PathBuf};

#[cfg(windows)]
const LINE_ENDING: &'static str = "\r\n";
#[cfg(windows)]
const LINE_ENDING_ESCAPED: &'static str = "\\r\\n";
#[cfg(windows)]
const LINE_ENDING_ESCAPED_REGEXP: &'static str = "\\\\r\\\\n";
#[cfg(not(windows))]
const LINE_ENDING: &'static str = "\n";
#[cfg(not(windows))]
const LINE_ENDING_ESCAPED: &'static str = "\\n";
#[cfg(not(windows))]
const LINE_ENDING_ESCAPED_REGEXP: &'static str = "\\\\n";

#[derive(Debug, Default)]
pub struct FileStorage {
    history_dir: PathBuf,
    passphrase: Option<String>,
}

fn escape_newline<T: AsRef<str>>(item: T) -> crate::Result<String> {
    let re =
        Regex::new(LINE_ENDING).map_err(|e| error::Error::Regex(LINE_ENDING.to_string(), e))?;
    let escaped = re.replace_all(&item.as_ref(), LINE_ENDING_ESCAPED);
    Ok(escaped.into_owned())
}

fn unescape_newline<T: AsRef<str>>(item: T) -> crate::Result<String> {
    let re = Regex::new(LINE_ENDING_ESCAPED_REGEXP)
        .map_err(|e| error::Error::Regex(LINE_ENDING_ESCAPED_REGEXP.to_string(), e))?;
    let unescaped = re.replace_all(&item.as_ref(), LINE_ENDING);
    Ok(unescaped.into_owned())
}

impl FileStorage {
    /// Gets a new instance with history stored at `history_dir`.
    pub fn new(history_dir: PathBuf) -> Self {
        FileStorage::default().history_dir(history_dir)
    }

    /// Sets passphrase used in encryption and decryption of history.
    pub fn passphrase<T: AsRef<str>>(mut self, passphrase: T) -> Self {
        self.passphrase = Some(passphrase.as_ref().to_string());
        self
    }

    /// Sets a dir where history files are stored.
    pub fn history_dir(mut self, mut history_dir: PathBuf) -> Self {
        history_dir.push("history_files");
        self.history_dir = history_dir;
        self
    }

    /// Generates and stores a name of the history file.
    fn store_history_name(&self, path: &Path) -> crate::Result<String> {
        use rand::distributions::Alphanumeric;
        use rand::{thread_rng, Rng};
        let id: String = thread_rng().sample_iter(&Alphanumeric).take(6).collect();
        let mut file = fs::OpenOptions::new()
            .write(true)
            .create(true)
            .open(&path)
            .map_err(|e| error::Error::IO(Some(path.to_path_buf()), e))?;
        write!(&mut file, "{}", &id).map_err(|e| error::Error::IO(Some(path.to_path_buf()), e))?;
        Ok(id)
    }

    /// Gets a filepath where the history file name is stored.
    fn holder_filepath(&self) -> crate::Result<PathBuf> {
        let mut holder_filepath =
            dirs::data_dir().ok_or(error::Error::Environment("Can't get path to Data Dir"))?;
        holder_filepath.push("klipbord");
        fs::create_dir_all(&holder_filepath)
            .map_err(|e| error::Error::IO(Some(holder_filepath.clone()), e))?;
        holder_filepath.push("history_name_holder");
        Ok(holder_filepath)
    }

    /// Gets a file name of the history file.
    ///
    /// The name is read if exists or it's generated and saved.
    fn history_filename(&self) -> crate::Result<String> {
        let holder_filepath = self.holder_filepath()?;
        let history_filename = if let Ok(id) = fs::read_to_string(&holder_filepath) {
            id
        } else {
            self.store_history_name(holder_filepath.as_path())?
        };
        Ok(history_filename)
    }

    /// Gets path to the history file.
    fn history_filepath(&self) -> crate::Result<PathBuf> {
        let mut path = self.history_dir.clone();
        fs::create_dir_all(&path).map_err(|e| error::Error::IO(Some(path.clone()), e))?;
        path.push(self.history_filename()?);
        Ok(path)
    }

    fn read_items(&self, path: &PathBuf) -> crate::Result<Vec<String>> {
        match fs::File::open(&path) {
            Err(err) if err.kind() == std::io::ErrorKind::NotFound => return Ok(Vec::new()),
            Err(err) => Err(error::Error::IO(Some(path.to_path_buf()), err)),
            Ok(mut file) => {
                let mut file_content = Vec::new();
                file.read_to_end(&mut file_content)
                    .map_err(|e| error::Error::IO(Some(path.to_path_buf()), e))?;
                let joined = if let Some(p) = &self.passphrase {
                    encryption::decrypt(&mut file_content, &p)?
                } else {
                    std::string::String::from_utf8(file_content)
                        .map_err(|e| error::Error::Conversion(e))?
                };
                joined.split(LINE_ENDING).map(unescape_newline).collect()
            }
        }
    }

    fn save(&self, items: &Vec<String>) -> crate::Result<()> {
        let joined = items.join(LINE_ENDING);
        let to_write = if let Some(p) = &self.passphrase {
            encryption::encrypt(joined, &p)?
        } else {
            joined.into_bytes()
        };
        let klipbord_file = self.history_filepath()?;
        let mut file = fs::OpenOptions::new()
            .create(true)
            .write(true)
            .open(&klipbord_file)
            .map_err(|e| error::Error::IO(Some(klipbord_file.clone()), e))?;
        file.write_all(to_write.as_ref())
            .map_err(|e| error::Error::IO(Some(klipbord_file), e))?;
        Ok(())
    }
}

impl Klipbord for FileStorage {
    fn insert_unique<T: AsRef<str>>(&self, item: T) -> crate::Result<()> {
        let item_string = item.as_ref().to_string();
        let mut all_items = self.read_items(&self.history_filepath()?)?;
        all_items.retain(|elem| elem != &item_string);
        let escaped = escape_newline(&item_string)?;
        all_items.push(escaped);
        self.save(&all_items)?;
        Ok(())
    }

    fn get_all(&self) -> crate::Result<Vec<String>> {
        fs::create_dir_all(&self.history_dir)?;
        let mut all_items = Vec::new();
        for entry in fs::read_dir(&self.history_dir)
            .map_err(|e| error::Error::IO(Some(self.history_dir.clone()), e))?
        {
            let path = entry?.path();
            let file_items = self.read_items(&path)?;
            all_items.extend(file_items);
        }
        Ok(all_items.into_iter().rev().collect::<Vec<String>>())
    }
}

#[cfg(test)]
mod test_file_klipbord {
    use super::*;
    use rand::distributions::Alphanumeric;
    use rand::{thread_rng, Rng};
    use std::env::temp_dir;

    fn random_tmp_dir() -> PathBuf {
        let mut path = temp_dir();
        let sub_dir: String = thread_rng().sample_iter(&Alphanumeric).take(6).collect();
        path.push(sub_dir);
        path
    }

    #[test]
    //fn works_when_newline() -> Result<(), Box<dyn std::error::Error>> {
    fn works_when_newline() -> crate::Result<()> {
        let given = "a\nb";
        let klipbord = FileStorage::new(random_tmp_dir());
        // make sure we don't have any items from previous run
        fs::remove_file(klipbord.history_filepath()?).ok();
        klipbord.insert_unique(given)?;

        let found = klipbord.get_all()?;

        assert_eq!(found, vec![given]);
        Ok(())
    }

    #[test]
    fn skip_insert_when_already_present() -> crate::Result<()> {
        let given = "a\nb";
        let klipbord = FileStorage::new(random_tmp_dir());
        // make sure we don't have any items from previous run
        fs::remove_file(klipbord.history_filepath()?).ok();
        klipbord.insert_unique(given)?;
        klipbord.insert_unique(given)?;

        let found = klipbord.get_all()?;

        assert_eq!(found, vec![given]);
        Ok(())
    }

    #[test]
    fn insert_removes_previous_entry() -> crate::Result<()> {
        let klipbord = FileStorage::new(random_tmp_dir());
        // make sure we don't have any items from previous run
        fs::remove_file(klipbord.history_filepath()?).ok();
        klipbord.insert_unique("a")?;
        klipbord.insert_unique("b")?;
        klipbord.insert_unique("a")?;

        let found = klipbord.get_all()?;

        assert_eq!(found, vec!["a", "b"]);
        Ok(())
    }
}
