use druid::Size;
use druid::{
    BoxConstraints, Data, Env, Event, EventCtx, HotKey, KeyCode, LayoutCtx, LifeCycle, PaintCtx,
    UpdateCtx, Widget,
};

/// Makes a wrapped widget to be focused at start.
pub struct Focus<T: Data> {
    inner: Box<dyn Widget<T>>,
}

impl<T: Data> Focus<T> {
    pub fn new(inner: impl Widget<T> + 'static) -> Self {
        Self {
            inner: Box::new(inner),
        }
    }
}

impl<T: Data> Widget<T> for Focus<T> {
    fn event(&mut self, ctx: &mut EventCtx, event: &Event, data: &mut T, env: &Env) {
        self.inner.event(ctx, event, data, env);
        match event {
            Event::LifeCycle(LifeCycle::WindowConnected) => {
                ctx.request_focus();
            }
            Event::KeyDown(key_event) => match key_event {
                k_e if (HotKey::new(None, KeyCode::Escape)).matches(k_e) => {
                    dbg!("yo");
                    ctx.window().close();
                }
                _ => {}
            },
            _ => (),
        }
    }

    fn update(&mut self, ctx: &mut UpdateCtx, old_data: Option<&T>, data: &T, env: &Env) {
        self.inner.update(ctx, old_data, data, env);
    }

    fn layout(&mut self, ctx: &mut LayoutCtx, bc: &BoxConstraints, data: &T, env: &Env) -> Size {
        bc.debug_check("Focus");
        self.inner.layout(ctx, bc, data, env)
    }

    fn paint(&mut self, paint_ctx: &mut PaintCtx, data: &T, env: &Env) {
        self.inner.paint(paint_ctx, data, env);
    }
}
