use copypasta::{ClipboardContext, ClipboardProvider};
use std::fs;
use std::path::{Path, PathBuf};

pub mod cli;
pub mod config;
pub mod encryption;
pub mod error;
pub mod gui;
pub mod storage;
mod widgets;

type Result<T> = std::result::Result<T, error::Error>;

pub trait Klipbord {
    fn insert_unique<T: AsRef<str>>(&self, item: T) -> Result<()>;
    fn get_all(&self) -> Result<Vec<String>>;
}

trait SwapChannel {
    fn send(&self, text: &str) -> Result<()>;
    fn get(&self) -> Result<String>;
}

#[derive(Debug)]
pub struct FileChannel {
    path: PathBuf,
}

impl FileChannel {
    pub fn new<T: AsRef<Path>>(path: T) -> Self {
        FileChannel {
            path: path.as_ref().to_owned(),
        }
    }
}

impl SwapChannel for FileChannel {
    fn send(&self, text: &str) -> Result<()> {
        fs::write(&self.path, text)?;
        Ok(())
    }

    fn get(&self) -> Result<String> {
        let found = fs::read_to_string(&self.path)?;
        fs::remove_file(&self.path)?;
        Ok(found)
    }
}

pub fn record_loop(
    klipbord: storage::FileStorage,
    sleep_time: std::time::Duration,
    channel: FileChannel,
) -> std::result::Result<(), Box<dyn std::error::Error>> {
    let mut ctx: ClipboardContext = ClipboardContext::new()?;
    let mut last_item = String::new();
    loop {
        match channel.get() {
            Ok(text) => {
                dbg!(&text);
                if let Err(e) = ctx.set_contents(text.clone()) {
                    eprintln!("{:?}", e)
                } else {
                    last_item = text;
                }
            }
            Err(error::Error::IO(_, e)) if e.kind() == std::io::ErrorKind::NotFound => {}
            e @ _ => eprintln!("{:?}", e),
        }
        match ctx.get_contents() {
            Ok(new_item) => {
                if new_item != last_item {
                    dbg!(&new_item);
                    klipbord.insert_unique(&new_item)?;
                    last_item = new_item.clone();
                }
            }
            Err(e) => {
                ctx = ClipboardContext::new()?;
                eprintln!("{}", e);
            }
        }
        std::thread::sleep(sleep_time);
    }
}
