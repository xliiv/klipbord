use klipbord;
use klipbord::cli;
use klipbord::config;
use klipbord::encryption;
use klipbord::gui;
use std::path::PathBuf;
use structopt::StructOpt;

static RECORD_INTERVAL_MS: u64 = 500;

fn main() {
    if let Err(err) = run() {
        eprintln!("{}", err);
        std::process::exit(2);
    }
}

fn run() -> Result<(), Box<dyn std::error::Error>> {
    let opt = cli::Cli::from_args();
    let data_path = data_path().expect("Can't get default data dir");

    let mut channel_path = data_path.clone();
    channel_path.push("communication_channel");

    dbg!(&opt);
    let config = config::Config::read_or_create(
        &config_path().expect("Can't get default config file path"),
        config::Config {
            syncer_dir: data_path,
            encryption: false,
            record_interval_ms: std::time::Duration::from_millis(RECORD_INTERVAL_MS),
        },
    )?;
    dbg!(&config);

    let kbord = if config.encryption {
        klipbord::storage::FileStorage::new(config.syncer_dir)
            .passphrase(encryption::read_passphrase()?)
    } else {
        klipbord::storage::FileStorage::new(config.syncer_dir)
    };

    match opt {
        cli::Cli {
            recording: true, ..
        } => {
            klipbord::record_loop(
                kbord,
                config.record_interval_ms,
                klipbord::FileChannel::new(&channel_path),
            )?;
        }
        cli::Cli { select: true, .. } => {
            gui::show_gui(kbord, klipbord::FileChannel::new(&channel_path));
        }
        _ => {
            let mut clap = cli::Cli::clap();
            clap.print_long_help()?;
        }
    }

    Ok(())
}

fn config_path() -> Option<PathBuf> {
    dirs::config_dir().and_then(|mut p| {
        p.push("klipbord");
        p.push("klipbord.toml");
        Some(p)
    })
}

fn data_path() -> Option<PathBuf> {
    dirs::data_dir().and_then(|mut p| {
        p.push("klipbord");
        Some(p)
    })
}
