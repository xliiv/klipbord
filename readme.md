# Klipbord

> A clipboard manager.

## Project status

1. [x] Make it work
2. [x] Make it right-ish
3. [ ] Make it fast

## TODO
- publish repo
    - CI for repo
- handle clipboard types via meta
    - copying files is broken, because files are casted to utf-8 (paths)
    - image support
- use polling? (https://github.com/xliiv/clipster/blob/18cee4fb6323480ab516fcdb2d4969492ddd7d21/clipster#L707)
    - what about interoperability?
    - klipbord sometimes overwrites citrix clipboard, because recording is
      faster than citrix
- replace `dbg!` with logger
    - https://github.com/BurntSushi/ripgrep/blob/master/src/logger.rs#
- there is a sleep when a user selects an item from history (GUI) - replace it
  with e.g. sytem pipe
- allow only one instance of `klipbord --recording`
    - using heartbeat
    - .. or https://stackoverflow.com/questions/27469928/is-there-file-locking-in-rust
- better key support
    - ESC closes the window
    - Ctrl-n moves selection one row down
    - Ctrl-p moves selection one row up
    - Enter selects item and closes the window
- build and publish .deb & .rpm (https://github.com/japaric/trust ?)
    - systemctl template
- filtering uses smart-case
- cross-platform
    - Windows
    - MacOs
    - Linux Wayland
- show GUI errors on GUI
- if `klipbord --recording` is not running, `klipbord --select` should either
    - inform user and exit
    - inform user and run
    - inform user and run `klipbord --recording` in the background
- use Druid some kind of tick event to update list while the select window is open
