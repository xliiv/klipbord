#!/usr/bin/env bash

set -ex

rustc --version
cargo --version
cargo test --verbose --release
