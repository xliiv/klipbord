#!/usr/bin/env python
import os
import pathlib
import random

FILE_DIR = pathlib.Path(__file__).parents[0]


def save_lines(lines, dst):
    """Write `lines` to file at `dst`."""
    with open(dst, 'w') as hdlr:
        content = os.linesep.join(lines)
        hdlr.write(content)


def generate_text(words, min_length, delimeter=' '):
    """Generates text using `words` joined with `delimeter`"""
    text = ''
    text_len = 0
    while text_len < min_length:
        new_word = random.choice(words)
        text = delimeter.join([text, new_word])
        text_len += len(new_word) + len(delimeter)
        #print(f"Text generation length: {text_len}/{min_length}")
    return text


def filter_words(words, inc_min, exc_max):
    """Filter word lists to match length between `inc_min` and `exc_max`."""
    word_file = "/usr/share/dict/words"
    words = words or open(word_file).read().splitlines()
    return [word for word in words if inc_min <= len(word) < exc_max]


def generate_file(dst, entry_count, text_length, word_min_inc, word_max_exc, all_words, skip=True):
    if skip == True and dst.exists():
        return
    print(f"Generating file {dst!r}")
    words = filter_words(all_words, word_min_inc, word_max_exc)
    lines = []
    for i in range(entry_count):
        lines.append(generate_text(words, text_length))
        print(f"File generation lines: {i}/{entry_count}")
    save_lines(lines, dst)


def main():
    word_file = "/usr/share/dict/words"
    with open(word_file) as hdlr:
        all_words = hdlr.read().splitlines()

    for text_length in (6, 3483):
        for entry_count in (10, 1024):
            file_name = f'len_{text_length}_entries_{entry_count}.dat'
            generate_file(
                dst=FILE_DIR / file_name,
                text_length=text_length,
                entry_count=entry_count,
                word_min_inc=6,
                word_max_exc=7,
                all_words=all_words,
            )


if __name__ == "__main__":
    main()
