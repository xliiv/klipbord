use criterion::{black_box, criterion_group, criterion_main, Criterion};
use std::env;
use std::fs;
use std::path::PathBuf;
use std::str::FromStr;

use klipbord::storage::FileStorage;
use klipbord::Klipbord;

fn prepare(
    history_dir_name: &str,
    data_name: &str,
) -> Result<FileStorage, Box<dyn std::error::Error>> {
    let mut history_dir = env::temp_dir();
    history_dir.push(history_dir_name);
    fs::remove_dir_all(&history_dir).ok();
    let storage = FileStorage::new(history_dir);

    let data_path = format!("benches/data/{}.dat", data_name);
    let content = fs::read_to_string(PathBuf::from_str(&data_path)?)?;
    for word in content.split('\n') {
        storage.insert_unique(&word)?;
    }
    Ok(storage)
}

fn insert_unique_1(c: &mut Criterion) {
    let data_dir = "len_6_entries_10";
    let bench = format!("{}_encrypted", data_dir);
    let storage = prepare(&bench, data_dir)
        .expect("Can't get FileStorage")
        .passphrase("some-passphrase");

    c.bench_function(&bench, |b| {
        b.iter(|| storage.insert_unique(black_box("some-value".to_string())))
    });
}

fn insert_unique_2(c: &mut Criterion) {
    let data_dir = "len_6_entries_1024";
    let bench = format!("{}_encrypted", data_dir);
    let storage = prepare(&bench, data_dir)
        .expect("Can't get FileStorage")
        .passphrase("some-passphrase");

    c.bench_function(&bench, |b| {
        b.iter(|| storage.insert_unique(black_box("some-value".to_string())))
    });
}

fn insert_unique_3(c: &mut Criterion) {
    let data_dir = "len_3483_entries_10";
    let bench = format!("{}_encrypted", data_dir);
    let storage = prepare(&bench, data_dir)
        .expect("Can't get FileStorage")
        .passphrase("some-passphrase");

    c.bench_function(&bench, |b| {
        b.iter(|| storage.insert_unique(black_box("some-value".to_string())))
    });
}

fn insert_unique_4(c: &mut Criterion) {
    let data_dir = "len_3483_entries_1024";
    let bench = format!("{}_encrypted", data_dir);
    let storage = prepare(&bench, data_dir)
        .expect("Can't get FileStorage")
        .passphrase("some-passphrase");

    c.bench_function(&bench, |b| {
        b.iter(|| storage.insert_unique(black_box("some-value".to_string())))
    });
}

fn get_all_1(c: &mut Criterion) {
    let data_dir = "len_6_entries_10";
    let bench = format!("{}_encrypted", data_dir);
    let storage = prepare(&bench, data_dir)
        .expect("Can't get FileStorage")
        .passphrase("some-passphrase");

    c.bench_function(&bench, |b| b.iter(|| storage.get_all()));
}

fn get_all_2(c: &mut Criterion) {
    let data_dir = "len_6_entries_1024";
    let bench = format!("{}_encrypted", data_dir);
    let storage = prepare(&bench, data_dir)
        .expect("Can't get FileStorage")
        .passphrase("some-passphrase");

    c.bench_function(&bench, |b| b.iter(|| storage.get_all()));
}

fn get_all_3(c: &mut Criterion) {
    let data_dir = "len_3483_entries_10";
    let bench = format!("{}_encrypted", data_dir);
    let storage = prepare(&bench, data_dir)
        .expect("Can't get FileStorage")
        .passphrase("some-passphrase");

    c.bench_function(&bench, |b| b.iter(|| storage.get_all()));
}

fn get_all_4(c: &mut Criterion) {
    let data_dir = "len_3483_entries_1024";
    let bench = format!("{}_encrypted", data_dir);
    let storage = prepare(&bench, data_dir)
        .expect("Can't get FileStorage")
        .passphrase("some-passphrase");

    c.bench_function(&bench, |b| b.iter(|| storage.get_all()));
}

criterion_group!(
    benches,
    // TODO: organize benchmarks like here
    // https://github.com/bheisler/criterion.rs/blob/b92fef5e06dc138a17b7f87b62a8a7de0813501c/benches/bench_main.rs#L5
    insert_unique_1,
    insert_unique_2,
    insert_unique_3,
    insert_unique_4,
    get_all_1,
    get_all_2,
    get_all_3,
    get_all_4,
);
criterion_main!(benches);
