export APP_NAME=klipbord
export COMPILED_BIN=target/release/klipbord
export COMPILED_BIN_ABS=`readlink -f $(COMPILED_BIN)`
export INSTALL_DIR=~/.local/bin
export INSTALL_PATH=$(INSTALL_DIR)/$(APP_NAME)
# Service
export SERVICE_CONFIG_PATH=systemd/klipbord.service
export SERVICE_DIR=~/.config/systemd/user
export SERVICE_NAME=$(APP_NAME).service
export SERVICE_DIR_PATH=$(SERVICE_DIR)/$(SERVICE_NAME)


build:
	cargo build --release

install: build uninstall
	cp $(COMPILED_BIN) $(INSTALL_DIR)

uninstall:
	touch $(INSTALL_PATH)
	rm -rf $(INSTALL_PATH)

service-install: build install
	mkdir -p $(SERVICE_DIR)
	cp $(SERVICE_CONFIG_PATH) $(SERVICE_DIR_PATH)
	systemctl --user daemon-reload
	systemctl enable --user $(SERVICE_NAME)
	systemctl start --user $(SERVICE_NAME)
	systemctl status --user $(SERVICE_NAME)

service-uninstall:
	systemctl stop --user $(SERVICE_NAME)
	systemctl disable --user $(SERVICE_NAME)
	rm -rf $(INSTALL_PATH)
	rm -rf $(SERVICE_DIR_PATH)

install-dev: build uninstall
	ln -st $(INSTALL_DIR) $(COMPILED_BIN_ABS)

bench-input: 
	python benches/data/generate_data.py

bench-run: bench-input
	cargo bench -- --verbose
