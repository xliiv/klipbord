# Benchmarking Rust

Rust supports benchmarking via `cargo bench`.
However, it's still unstable. A recommened crate for some people is `criterion`.

See Discord discussion


> xliiv Last Wednesday at 9:07 PM
> i mean this https://play.rust-lang.org/?version=nightly&mode=debug&edition=2015&gist=6dadbeba570fec9af40fbdaa21fc69cb
> 
> shepmaster Last Wednesday at 9:10 PM
> @xliiv note you are reading the docs for 1.7. We are at 1.41 today.
> @xliiv https://play.rust-lang.org/?version=nightly&mode=debug&edition=2015&gist=cd377d6992901cd7ed6f950bb4e97f08
> 
> xliiv Last Wednesday at 9:11 PM
> @shepmaster O_o damn google, stop abusing my ignorance :slight_smile:
> 
> shepmaster Last Wednesday at 9:11 PM
> The problem is that version of the book put the contents of the code in a main when it shouldn't have
> 
> xliiv Last Wednesday at 9:12 PM
> @shepmaster thanks for the link - works. 
> 1. does it mean i have to use nightly?
> 2. is there a chapter in the book dedicated to benches?
> 
> ThatOneLutenist Last Wednesday at 9:13 PM
> the built in benchmark framework is nightly only
> 
> shepmaster Last Wednesday at 9:13 PM
> yes, built-in benchmarking is still unstable. It might not be in the book anymore for the same reason.
> Use criterion though
> 
> ThatOneLutenist Last Wednesday at 9:13 PM
> criterion is an excellent replacement
> 
> shepmaster Last Wednesday at 9:13 PM
> https://docs.rs/criterion/0.3.1/criterion/
> 
> xliiv Last Wednesday at 9:15 PM
> @ThatOneLutenist & @shepmaster thanks for the criterion i'll check it


## Critierion

### Reading output

https://bheisler.github.io/criterion.rs/book/user_guide/command_line_output.html

# Benchmarking Klipbord

I'd like to profile these parts of `klipbord`
1. `insert_unique`: writing item to the clipboard history
2. `get_all`: getting all items from the recorded clipboard history

## `insert_unique`

I can see these params

1. clipboard item length
    1. short entry: 6 chars word
    2. long entry: 5 paragraph of Lorem Ipsum = 3483 of chars/bytes
        See https://www.lipsum.com/feed/html
2. clipboard item history count
    1. few: 10 entries
    2. many: 1024 entries (TODO: implement max history length based on the performance)
3. with/without encryption - **WE SHOULD RATHER TEST ONLY ENCRYPTED VARIANT?**
    1. encrypted history
    1. plain history

## `get_all`

Use the same use cases as [`insert_unique`](#`insert_unique`)
