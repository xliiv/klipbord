xliiv: Hey,
i'd like to share an odd example of evt_ctx.window().close(); use
The code is here
https://github.com/xi-editor/druid/compare/master...xliiv:close-window
and these are how it look like..

    working main (https://github.com/xi-editor/druid/compare/master...xliiv:close-window#diff-4d2a58822f247f0e07a2a3eeda303a82R8)
    druid-close-works.gif

    broken main (https://github.com/xi-editor/druid/compare/master...xliiv:close-window#diff-4d2a58822f247f0e07a2a3eeda303a82R19)
    durid-close-broken.gif

xliiv: any idea maybe? :)

Colin Rofls: oh yea that's not the API for window closing

Colin Rofls: one sec

Colin Rofls: basically you need to send a CLOSE_WINDOW command

Colin Rofls: or else druid doesn't get notified that the window is going away

Colin Rofls: I agree we should have some better mechanism for this

xliiv: so i should send a command with CLOSE_WINDOW in the button code?

Colin Rofls: yea :)

xliiv: i'm not familar with the subject, but tomorrow i'm going to read about commands

xliiv: thanks @Colin Rofls

Colin Rofls: it's just ctx.submit_command(druid::commands::CLOSE_WINDOW, None)

Colin Rofls: that should work. :)

xliiv: it doesn't :/
perhaps i did something wrong, i'll double-check tomrrow
anyway thanks again @Colin Rofls :)

Colin Rofls: okay, have a good night!

xliiv: Confirmed, doesn't work :/

Colin Rofls: hmm

Colin Rofls: what platform? if you run the multiwin example, can you close windows with ctrl/cmd+w?

xliiv: Linux

xliiv: A Simple window application perhaps the loop is problematic?

Colin Rofls: I think there's a single window example that has the ctrl+w behaviour, lemme check

xliiv: did you see this code? https://github.com/xi-editor/druid/compare/master...xliiv:close-window
there are 2 main fns

    one works
    one doens't, guess it's a problem with objects dropping since there is a weird loop?

Colin Rofls: huh, what're you trying to do here?

xliiv: it's not complete the code but the essence.. i'm using global hotkey registaration to build druid application :)
it's my way of walking around that druid doesn't support task bar like programs

xliiv: soon i'll pulish it so i will be albe to just paste a link to it

Colin Rofls: gotcha

Colin Rofls: okay, so the close button still works in your example for me

xliiv: hmm, i'll recompile it

Colin Rofls: oh: one thing you might run into is that the application doesn't quit when the window is closed?

Colin Rofls: (i'm on mac though)

xliiv: Linux here

Colin Rofls: kk I can test there too

Raph Levien: Are you getting messages that a try borrow is being dropped? I was originally going to guess Windows, where that's definitely a risk.

    xliiv:

    Are you getting messages that a try borrow is being dropped?

Don't see anything like that
This looks like here
druid-close-broken2.gif

Colin Rofls: oh: I think your program is working as written?

Colin Rofls: nothing terminates your loop

Colin Rofls: so even if the application quits, you're still in an infinite loop

xliiv: That's correct but I'm expecting the window to be closed by a click
And it's not

xliiv: Looks like this need some debugging so I'm going to leave it for now

Colin Rofls: yea, that is slightly weird; this looks like an issue in the gtk backend, and I'm not totally sure what's going on. It may be that we don't dispose of the root window handle until the process terminates or something.

Colin Rofls: yea, there's something strange going on; you could open an issue if you like?

xliiv: If I put break into the loop the window is dropped like expected

xliiv: I'll open one
Eventually I'd like to fix it but it's not priority right now

xliiv: Done https://github.com/xi-editor/druid/issues/464
