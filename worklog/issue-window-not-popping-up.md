# Problem
The hotkey doesn't bring up the window for Gnome3. Instead, it shows
a notifacation, like 'Window is ready'.

# Solution
Install this plugin

https://extensions.gnome.org/extension/234/steal-my-focus/

# Similar solution

https://askubuntu.com/questions/80969/gnome-shell-move-windows-to-front-on-launch-no-more-is-ready-to-use-noti
