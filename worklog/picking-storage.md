Currently, using a simple file.
In future, one of [Alternatives](#Alternatives) should be picked

# Alternatives

* https://github.com/jgallagher/rusqlite
* https://github.com/rust-rocksdb/rust-rocksdb
* https://github.com/spacejam/sled


Which pick?

According to https://github.com/spacejam/sled#known-issues-warnings

> * if reliability is your primary constraint, use SQLite. sled is beta.
> * if storage price performance is your primary constraint, use RocksDB. sled uses too much space sometimes.
> * quite young, should be considered unstable for the time being.
> * the on-disk format is going to change in ways that require manual migrations before the `1.0.0` release!
> * until `1.0.0`, sled targets the *current* stable version of rust. after `1.0.0`, we will aim to trail current by at least one version. If this is an issue for your business, please consider helping us reach `1.0.0` sooner by financially supporting our efforts to get there.
