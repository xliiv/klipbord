# sqlite problem

## Error

```
.. cut ..
  = note: /usr/bin/ld: cannot find -lsqlite3
            collect2: error: ld returned 1 exit status
.. cut ..
```

## Solution
For Fedora install missing packages
```
libsqlite3x-devel.x86_64
```
