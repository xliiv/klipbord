# sqlite problem

## Error

```
> cargo run --example test
   Compiling libudev-sys v0.1.4
error: failed to run custom build command for `libudev-sys v0.1.4`

Caused by:
  process didn't exit successfully: `/home/xliiv/workspace/_can_be_deleted/InputBot/target/debug/build/libudev-sys-101fc70f26d621b3/bui
ld-script-build` (exit code: 101)
--- stderr
thread 'main' panicked at 'called `Result::unwrap()` on an `Err` value: "`\"pkg-config\" \"--libs\" \"--cflags\" \"libudev\"` did not e
xit successfully: exit code: 1\n--- stderr\nPackage libudev was not found in the pkg-config search path.\nPerhaps you should add the di
rectory containing `libudev.pc\'\nto the PKG_CONFIG_PATH environment variable\nPackage \'libudev\', required by \'virtual:world\', not
found\n"', src/libcore/result.rs:1165:5
note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace.
```

## Solution
For Fedora install missing packages
```
sudo dnf install systemd-devel.x86_64
```

