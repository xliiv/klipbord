# Rust GUI

## Overview

https://areweguiyet.com/newsfeed/2019-01-13_rust2019.html

## Potential Solutions

* https://github.com/redox-os/orbtk
* https://github.com/xi-editor/druid
* https://github.com/maps4print/azul
* https://github.com/PistonDevelopers/conrod

Also, found somewhere `iced` crate
* https://github.com/hecrj/iced

### Conclusion

Picked only 2 crates for further comparison.

* https://github.com/xi-editor/druid
* https://github.com/hecrj/iced

The rest seems to be abandoned.


## Iced

Pros (including opinions):
    1. Good for a single view programs
    2. Decent look?
    3. Good examples which make easy to start quickly with the library

Cons (including opinions):
    1. Doesn't feel like developing "traditional" desktop program (menubar, etc.)
    2. One-man project?
    3. Bad for non-single view apps? (I don't like how the wizard code look like https://github.com/hecrj/iced/blob/master/examples/tour/src/main.rs)

## Druid

Pros (including opinions):
    1. Feels like developing "traditional" desktop app (e.g. multi windows)
    2. More people involved in the development (including Raph Linus, creator of: xi-editor, pulldown-cmark used by librustdoc)
    3. Responsive and helpful live chat (Zulip)

Cons (including opinions):
    1. Look could be better
    2. Some usage examples should be more intuitive

## Druid vs Iced

I need excatly the opposite of `Iced` Cons. Suprisingly, this is `Druid`, so
the choice is easy.

## Druid Topics

https://xi.zulipchat.com/#narrow/stream/147926-druid/sender/254915-tymoteusz.jankowski
