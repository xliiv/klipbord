> xliiv: Hi, is it possible to create a taskbar application now? like a clipboard manager which
> 
>     listen to a hotkey
>     its main window is hidden
>     shows an icon in the taskbar
>     ?
> 
> Colin Rofls: no, this is not currently possible, although I think it's actually a really good use-case.
> 
> Colin Rofls: our architecture currently expects there to be a window, and how taskbar/statusbar apps work is fairly different between platforms; someone will at some point need to dive into this. (it could certainly be you!)
> 
> xliiv: nice, i dont say no. is there a task for this which i could follow or even assign if i decide?
> 
> Colin Rofls: let me look. there's nothing specific to this, but there is some related stuff about having a top-level application object; this is important on mac because mac applications are expected to work without any windows being open.
> 
> Colin Rofls: https://github.com/xi-editor/druid/issues/114
> 
> Colin Rofls: I also recall talking to @matt rice about this at some point, but I forget where that was? :confused:
> 
> xliiv: no prob.
> thanks for the link
> 
> matt rice: I only recall discussing it in that issue linked above.
> 
> Benjamin Saunders: is there a standard way to register a global hotkey in wayland at all?
> 
> xliiv: i use wayland and succesfully registered a hotkey with this crate
> https://github.com/LiveSplit/livesplit-core/tree/master/crates/livesplit-hotkey
> @Benjamin Saunders with this snippet
> 
> fn main() -> Result<(), Box<dyn std::error::Error>> {
>     use livesplit_hotkey::{Hook, KeyCode};
>     use std::{thread, time};
> 
> 
>     let hook = Hook::new().unwrap();
>     hook.register(KeyCode::Space, || println!("YO")).unwrap();
>     while true {
>         println!("xxx");
>         thread::sleep(time::Duration::from_millis(1000));
>     }
> }
> 
> Benjamin Saunders: that looks like it's using X11, so it's not something I'd want to rely on long term
> 
> xliiv: this also contains some interesting content
> https://github.com/enigo-rs/enigo/issues/6

from https://xi.zulipchat.com/#narrow/stream/147926-druid/topic/taskbar.20application.20with.20hotkey
