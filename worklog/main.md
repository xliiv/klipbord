# Clipboard

## [rust-clipboard](https://github.com/aweinstock314/rust-clipboard)

Turns out that `rust-clipboard` suffers two problems

1. wayland support
2. cleared clipboard

### [Wayland support](https://github.com/aweinstock314/rust-clipboard/pull/64)

A few merge request have been done, but none of them is merged

1. https://github.com/aweinstock314/rust-clipboard/pulls?q=is%3Apr+is%3Aopen+sort%3Aupdated-desc
    1. https://github.com/aweinstock314/rust-clipboard/pull/65
    2. https://github.com/aweinstock314/rust-clipboard/pull/71
    3. https://github.com/aweinstock314/rust-clipboard/pull/64


### [Cleared clipboard](https://github.com/aweinstock314/rust-clipboard/issues/61)

And this is by design ..

1. https://github.com/aweinstock314/rust-clipboard/issues/61#issuecomment-462449918
2. https://www.uninformativ.de/blog/postings/2017-04-02/0/POSTING-en.html

## [copypasta](https://github.com/alacritty/copypasta)

Of course, there is a `rust-clipboard` fork called `copypasta`.
Although it adds Wayland support, it keeps the cleared clipboard problem, see

https://github.com/alacritty/copypasta/issues/5

## [wl-clipboard-rs](https://github.com/YaLTeR/wl-clipboard-rs)

It supports only Wayland. `Klipbord` should be cross-platform, so it needs a
cross-platform library.

## [smithay-clipboard](https://github.com/smithay/smithay-clipboard)

The purpose of this library is to support programs with GUI, see

> You need surface around to handle KbdEnter etc and actually draw in it.
> Without it it won't work. If your use case is CLI usage, this crate isn't for
> you and you should look at YaLTeR/wl-clipboard-rs .
> 
>     Btw, without the WAYLAND_DISPLAY=/run/user/1000/wayland-0 env variable
>     set I'm getting
> 
> Your wayland compositor should set WAYLAND_DIPLAY, so check env | grep
> WAYLAND_DISPLAY.
> 
> Update:
> So, I'd recommend you to start from Smithay/client-toolkit:examples@master
> once you drawn your surface and start handling events, etc. you can use
> clipboard from here.(smithay-clipboard should start working after KbdEnter
> event IIRC).

from https://github.com/Smithay/smithay-clipboard/issues/11

## Decision

It turns out that the cleared clipboard can be easily overcome by taking over the
ownership of the copied item by the deamon.

The best match is `copypasta`, because

1. `copypasta` supports Wayland (contrary to `rust-clipboard`)
2. the only problem with `copypasta` is the cleared clipboard problem  (or
   rather it's a design of X11).
   However, it can be easily fixed by additional `set_content` in the deamon
   version of `klipbord`


# Tray Icon

See src/ui_tray_icon.rs on
https://github.com/hasufell/pnmixer-rust/commit/ee847ff896ad0c467e9bd9d30dd5e29668eea7be#diff-3d17d76419e53d69001d7647f508e698L535


# Encryption

Handled in [a seperate file](./picking-encryption.md).


# Hotkey

LiveSplit choice needs rethinking, because LiveSplit
1. lacks support of key compination, see below
2. lacks Wayland support, see below

So, a new approach should be picked from
1. fix both issues in LiveSplit
2. change the hotkey library
3. fix `druid`, so `druid` supports task-bar-AND-hot-key applications

## Key combination for livesplit-core

Issue https://github.com/LiveSplit/livesplit-core/issues/280

## Wayland support

Issue https://github.com/LiveSplit/livesplit-core/issues/140

## Implementations in different libs

1. https://github.com/obv-mikhail/InputBot
2. https://github.com/UnwrittenFun/hotkey-rs
3. https://github.com/rkarp/winapi-easy

## Decision

It turns out that the best appraoch is to delegate the key registration to a
window manager. This needs a simple change of the overall design.
Split the program into two parts

1. service (e.g. `klipbord --daemon`)that collects clipboard history
2. GUI (e.g. `klipbord`) that allows to pick from clipboard history

Instead of previous, run a-single-process-that-does-both.


# Daemon

https://github.com/xi-editor/druid/issues/114

## Daemon in Rust

1. https://github.com/jwilm/rust_daemon_template
2. https://github.com/paritytech/parity-daemonize
3. https://github.com/knsd/daemonize

## Daemon in Go
https://fabianlee.org/2017/05/21/golang-running-a-go-binary-as-a-systemd-service-on-ubuntu-16-04/


## Daemon in Python
https://pagure.io/python-daemon/blob/master/f/daemon


# Packaging

## Debian

1. https://lib.rs/crates/cargo-deb
2. https://blog.hackeriet.no/packaging-a-rust-project-for-debian/


# Image support

## Wayland

>  `copypasta` isn't a crate you want to create a clipboard manager, at least on
> Wayland. it's designed to work in pair with other gui toolkits, winit, etc, not
> to run in CLI. Also, please, test your code natively on X11, since XWayland is
> strange sometimes.

from https://github.com/alacritty/copypasta/issues/6#issuecomment-595600299


Looks like Wayland support will need another redesign O_o
Maybe even using druid-as-task-bar-application, see
https://github.com/xi-editor/druid/issues/114


https://github.com/YaLTeR/wl-clipboard-rs

### File locking

https://stackoverflow.com/questions/27469928/is-there-file-locking-in-rust
