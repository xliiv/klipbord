# Best match

## Update

So the recommended choice would be
1. [ring](https://github.com/briansmith/ring) (usage example: https://github.com/elichai/Lorenz/blob/master/src/encryption.rs)
2. [RustCrypto](https://github.com/RustCrypto/AEADs) (especially: https://docs.rs/aes-gcm/0.3.0/aes_gcm/#usage)

From https://rust-lang.zulipchat.com/#narrow/stream/122651-general/topic/encrypt.20local.20files

> xliiv: Any crate for encrypting files with a password?
> 
> xliiv: potential solution https://docs.rs/openssl/0.10.26/openssl/aes/index.html
> 
> xliiv: another one
> https://docs.rs/themis/0.12.0/themis/secure_cell/index.html
> 
> xliiv: another one https://docs.rs/aes-ctr/0.3.0/aes_ctr/
> 
> Elichai Turkel: Please keep in mind that you must use authenticated encryption (ie aes-gcm / chacha20-poly1305)
> I'd either use ring or the aead crate
> 
> xliiv: just learnt what authenticated encryption is, thanks.
> makes sense, and i'm going to use.
> however, https://docs.rs/ring/0.16.9/ring/index.html looks a bit too low-level.. or at least assumes more encryption knowledge than i have..
> any example how to use it?
> 
> xliiv: found it? https://gist.github.com/jaysonsantos/117c1f7623a2fb2c39e47f6cf87860a3
> 
> xliiv: can't use Ring crate.. there is no example which could explain it to me
> https://github.com/briansmith/ring/issues/418
> and the one i found is outdated
> 
> xliiv: aead crate (https://github.com/RustCrypto/traits) is also not what i needed, it's some sort of trait collection.. did i found what you're talking about?
> 
> xliiv: @Elichai Turkel
> 
> Elichai Turkel: Yeah ring is somewhat messy.
> Look at the table here: https://github.com/RustCrypto/AEADs/blob/master/README.md
> My recommendation is if it's meant to run on x86 use aes-gcm if also phones/low resource devices then use chacha20-poly1305 (the first has hardware implementations while the latter is really fast in software)
> 
> Elichai Turkel: If you do want to use ring this is something I wrote a year or so ago which you can look at as example:
> 
> https://github.com/elichai/Lorenz/blob/master/src/encryption.rs
> 
> xliiv: finally something speaks my tongue
> i'm a bit concerned about
> 
>     the content of this https://github.com/RustCrypto/AEADs/tree/master/aes-gcm#security-warning
>     the count of contributors - only 4
> 
> but since i'm working on a toy project, i can live with that..
> 
> anyway thanks @Elichai Turkel it helps a lot
> 
> Elichai Turkel: Yep. That's why I proposed ring, I have more trust in ring than that.
> On the other hand it's harder to screw up chacha so that might be a better fit
> 
> xliiv: ok, then i'll give ring another chance since i've got a new input which is your Lorenz example
> 
> Elichai Turkel: Ping if you need any help :)
> 
> xliiv: @Elichai Turkel could you look at this code and tell if it's wrong?
> https://github.com/briansmith/ring/compare/master...xliiv:aead-example?expand=1
> 
> Plus, there are 2 questions, see code comments, do you have answers maybe?
> 
> xliiv: i know you decrypt a bit diffrently in Lorenz, but i couldn't apply your way.. because i was getting error when creating OpeningKey
> 
>     Elichai Turkel:
>     PBKDF2 salt should be random IIRC probably best to read the RFC
> 
>     Elichai Turkel:
>     About rand_vec the nonce can be public and is usually prepended to the cipher text
> 
>     Elichai Turkel:
>     About the last question open_in_place returns you the exact slice that contains the decrypted data, you can either use it or use it to retrieve the length and shorten your Vec
> 
>     xliiv:
> 
>     About the last question open_in_place returns you the exact slice that contains the decrypted data, you can either use it or use it to retrieve the length and shorten your Vec
> 
> works like a charm - thank you!
> 
>     probably best to read the RFC
> 
> although, i'm not compatible with RFCs (are too generic i prefer code), this one somewhat helped - thanks again!
> 
>     prepended to the cipher text
> 
> i'm going to use it (when i was experimenting with your way of decryption i failed. now i know why.. i only adopted your decryption and not encryption which is why the decryption failed)
> 
>     xliiv:
> 
>     i'm going to use it (when i was experimenting with your way of decryption i failed. now i know why.. i only adopted your decryption and not encryption which is why the decryption failed)
> 
> yep, confirmed.. i adopted Lorence's both encrypt AND decrypt and now it fucking works! \o/
> HUGE thanks @Elichai Turkel
> 
> Elichai Turkel: You're welcome :)


[themis](https://github.com/cossacklabs/themis)
especially [this ready to use file encryption example](https://docs.rs/themis/0.12.0/themis/secure_cell/index.html)


# Other choices

[Libhunt](https://rust.libhunt.com/ring-alternatives)


## [rage](https://github.com/str4d/rage)

Looks good especially
https://github.com/str4d/rage/blob/b2c4a5475107623b5c9b7b3699a9e383d590aef1/src/lib.rs#L77


## [openssl](https://github.com/sfackler/rust-openssl)

Looks good especially
https://docs.rs/openssl/0.10.26/openssl/aes/index.html


## [ring](https://github.com/briansmith/ring)

### Pros

### Cons
- no clear of doing a simple file encryption

### Some example

From https://play.rust-lang.org/?version=nightly&mode=debug&edition=2018&gist=beb07e7b716d4828f2f4de22a8cb3e2f


## [stream-ciphers](https://github.com/RustCrypto/stream-ciphers)

Overall, looks good.

Pros
- pure rust


## [cryptostream](https://github.com/neosmart/cryptostream)

Cons
- only 2 contributors


## [rust-crypto](https://github.com/DaGenix/rust-crypto)

Especially this example https://github.com/DaGenix/rust-crypto/blob/master/examples/symmetriccipher.rs

Cons
- last commit 2016


## [Cloaker](https://github.com/spieglt/Cloaker)

Contains cryption example https://github.com/spieglt/Cloaker/blob/729595eb315076e63f796c1561795f028358d190/core/src/lib.rs#L33
