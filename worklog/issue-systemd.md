```shell
> make service-install
cargo build --release
    Finished release [optimized] target(s) in 0.10s
sudo cp target/release/klipbord /usr/bin/klipbord
sudo cp systemd/klipbord.service /etc/systemd/system/klipbord.service
sudo systemctl enable klipbord.service
sudo systemctl start klipbord.service
sudo systemctl status klipbord.service
● klipbord.service - Records system clipboard content
   Loaded: loaded (/etc/systemd/system/klipbord.service; enabled; vendor preset: disabled)
   Active: failed (Result: exit-code) since Sat 2020-03-14 13:54:03 CET; 15ms ago
  Process: 78375 ExecStart=/usr/bin/klipbord --recording (code=exited, status=2)
 Main PID: 78375 (code=exited, status=2)

Mar 14 13:54:03 alef klipbord[78375]:     select: false,
Mar 14 13:54:03 alef klipbord[78375]: }
Mar 14 13:54:03 alef klipbord[78375]: [src/main.rs:29] &config = Config {
Mar 14 13:54:03 alef klipbord[78375]:     syncer_dir: "/home/xliiv/.local/share/klipbord",
Mar 14 13:54:03 alef klipbord[78375]:     encryption: false,
Mar 14 13:54:03 alef klipbord[78375]:     record_interval_ms: 500ms,
Mar 14 13:54:03 alef klipbord[78375]: }
Mar 14 13:54:03 alef klipbord[78375]: XCB connection error: ClosedParseErr
Mar 14 13:54:03 alef systemd[1]: klipbord.service: Main process exited, code=exited, status=2/INVALIDARGUMENT
Mar 14 13:54:03 alef systemd[1]: klipbord.service: Failed with result 'exit-code'.
make: *** [Makefile:30: service-install] Error 3
```

# Solution

> Autocutsel and an Introduction to Systemd User Services
> 28/09/2019 07:45 Permalink
> 
> This post shows how to run autocutsel as a systemd user service to keep X11 clipboards in sync.
> 
> X11 has two kinds of copy/paste buffers, namely the primary selection and the clipboard selection. The primary selection is invoked when highlighting text and with the middle mouse button while the clipboard selection uses Ctrl+C and Ctrl+V.
> 
> If you wish to keep both buffers in sync, you can use the autocutsel daemon to do just that, effectively merging the two buffers.
> User Service
> 
> I used to just start autocutsel with my window manager, but there is a more clean solution, systemd user services. User services are like regular system-wide services, but they are only started when the associated user is logged in and kept running until all sessions of that users are closed.
> 
>     As with regular systemd services, we need a service file.
> 
>     [Unit]
>     Description=Autocutsel
> 
>     [Service]
>     Type=forking
>     Restart=always
>     RestartSec=10
>     ExecStartPre=/usr/bin/autocutsel -fork
>     ExecStart=/usr/bin/autocutsel -selection PRIMARY -fork
> 
>     [Install]
>     WantedBy=session.target
> 
>     User service file go into the ~/config/systemd/user directory. We will save above service file as
> 
>     ~/.config/systemd/user/autocutsel.service
> 
>     To control user services, use systemctl like usual, but with the --user flag. After saving autocutsel.service, first reload the database of services with
> 
>     systemctl --user daemon-reload
> 
>     and then enable the service
> 
>     systemctl --user enable autocutsel
> 
>     Log out and back on again. autocutsel should be running and keeping your clipboards in sync.
> 
> systemd will make sure that only one instance of the service is running at a time and restart the daemon if it crashes.

from https://www.hacks.moe/posts/8/Autocutsel_and_an_Introduction_to_Systemd_User_Services


# Other Links
1. https://unix.stackexchange.com/questions/454935/trying-to-clear-the-clipboard
