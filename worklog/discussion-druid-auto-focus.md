xliiv: Any way of setting focus for a widget at start?

Zarenor: Widgets do not currently have any sort of lifecycle events. So you could one-shot something through it's parent, I suppose? If you wrote a custom wrapper to do that.
Lifecycle events are the focus of druid#404, which is one of they key areas of focus on the 0.5 roadmap

    xliiv:

    So you could one-shot something through it's parent, I suppose? If you wrote a custom wrapper to do that.

what do you mean exactly? i'd like to focus textbox in this example https://github.com/xi-editor/druid/compare/master...xliiv:filtering?expand=1#diff-f202ec579d7efe906048e47d72be3539R40
do you have any example of it that i could read?

Zarenor: I don't. I haven't yet needed to force focus anywhere. You would need to do it in an event, and presumably you would have a boolean to indicate whether you had already done it or not (since you only want to do it the first time)

xliiv: And where would be the entry point to achieve that?

xliiv: Should i write own AppDelegate? And use it here https://docs.rs/druid/0.4.0/druid/struct.AppLauncher.html#method.delegate

Zarenor: It would be pretty difficult to do it in an AppDelegate, since you presumably wouldn't have a reference to the TextBox you're wanting to focus. You need to write a struct implementing Widget, wrapping the TextBox you want to focus. That's probably currently the easiest.

xliiv: Any example of such widget wrapping to get the idea?

Zarenor: In that custom widget's event implementation, you should set it's child active, when the bool your custom widget has is not true. Then also set it true.

Zarenor: You could read through any of the widgets which wrap other widgets. Examples are: SizedBox, Align, EnvWrap,Split, Flex

Zarenor: (Probably in order of simplest to most complex)

xliiv: Awesome, thanks.. i'll give a try :+1:

xliiv: also :+1: for the order
thanks!
xliiv:

impl<T: Data> Widget<T> for Focus<T> {
    fn event(&mut self, ctx: &mut EventCtx, event: &Event, data: &mut T, env: &Env) {
        dbg!(&event);
        //dbg!(&ctx.window_id());
        if self.should_focus {
            ctx.request_focus();
            self.should_focus = false;
        }
        self.inner.event(ctx, event, data, env);
    }

did this, but the cursor is not displayed in the textbox..

xliiv: also tried with set_active(true)

    xliiv:
    ok, so calling a ctx.request_focus(); in the wrapper's event doesn't work
    but when i put ctx.request_focus(); in the event of textbox it works..
    any way of calling ctx.request_focus(); on the inner without changing the source code (like i did in 2)?

Paul Miller: I'm actually having a similar issue right now, not sure if it's related: I'm setting is_active and is_hot from a parent widget, but the child is only getting is_hot for some reason

Paul Miller: (trying to make a button that can hold any widget as its content)

Colin Rofls: the way to do this right now is to handle the existing Event::LifeCycle event, and request focus.

xliiv: Can you link an example of it or add some pseudo code to explain it better ? @Colin Rofls

    xliiv:

    to handle the existing Event::LifeCycle event

what does it mean, where should i do it? in the wrapper widget in the event fn?

    and request focus.

you mean to call request_focus (https://docs.rs/druid/0.4.0/druid/struct.EventCtx.html#method.request_focus)

Colin Rofls: yea, check out https://github.com/linebender/runebender/blob/master/src/widgets/editor.rs#L249

xliiv: i did this..

impl<T: Data> Widget<T> for Focus<T> {
    fn event(&mut self, ctx: &mut EventCtx, event: &Event, data: &mut T, env: &Env) {
        dbg!(&event);
        let edit: Option<Event> = match event {
            Event::LifeCycle(LifeCycle::WindowConnected) => {
                ctx.request_focus();
                None
            }
            _ => None,
        };
        self.inner.event(ctx, event, data, env);

    }

    fn update(&mut self, ctx: &mut UpdateCtx, old_data: Option<&T>, data: &T, env: &Env) {
        self.inner.update(ctx, old_data, data, env);
    }

    fn layout(&mut self, ctx: &mut LayoutCtx, bc: &BoxConstraints, data: &T, env: &Env) -> Size {
        bc.debug_check("Focus");
        self.inner.layout(ctx, bc, data, env)
    }

    fn paint(&mut self, paint_ctx: &mut PaintCtx, data: &T, env: &Env) {
        self.inner.paint(paint_ctx, data, env);
    }
}

but it suffer the same problem i described here
https://xi.zulipchat.com/#narrow/stream/147926-druid/topic/make.20a.20widget.20focused.20at.20start/near/184922170

Colin Rofls: ooooh, I think I know what's happening; if Focus<T> is using a WidgetPod then logic in the widget pod is suppressing the events, because it knows that the inner widget doesn't have focus.

Colin Rofls: In this case, what I would have is to not put the inner widget in a pod, and then the state from the current widget will just be passed through directly. In cases where layout/paint are passed through to the child widget without modification, you don't need widgetpod; widgetpod does stuff like handle and store layout information, such as when you have a list of widgets, and each one has different layout information.

xliiv: I'm wrapping textbox with Focus
Didn't get fully what you are proposing but maybe tomorrow I'll get it
Anyway thanks man :)

xliiv: btw @Colin Rofls i'm not putting my widget in the WidgetPod..
however i'm putting Textbox inside Focus, Focus inside Padding and Padding inside Flex::row

xliiv: ok padding uses widgetpod

xliiv: is this relevant?
xliiv:

fn build_widget() -> impl Widget<AppData> {
    Flex::column()
        .with_child(
            Flex::row().with_child(
                Padding::new(
                    5.0,
                    Focus::new(
                        TextBox::new().lens(lens::Id.map(
                            |d: &AppData| d.searched.clone(),
                            |d: &mut AppData, new_value: String| {
                                d.searched = new_value;
                                let items = (*d.klipbord.lock().unwrap()).get_all();
                                d.found = Arc::new(
                                    items
                                        .iter()
                                        .filter(|i| {
                                            if d.searched.as_ref() as &str == "" {
                                                return true;
                                            }
                                            i.contains(&d.searched)
                                        })
                                        .map(|s| s.to_string())
                                        .collect::<Vec<String>>(),
                                );
                            },
                        )),
                    )
                ),
                1.0,
            ),
            0.0,
        )
}

Paul Miller: (widgetpod was indeed my problem. not sure what's happening here though... might have something to do with the way the lens widget passes along events?)

Colin Rofls: yea, this might need some more serious debugging. If someone wants to point me at some runnable code I can take a look tomorrow? but I'll be flying much of the day so won't be very online.

xliiv: it's here https://github.com/xi-editor/druid/compare/master...xliiv:focus?expand=1
so running

git clone https://github.com/xliiv/druid.git
cd druid
git checkout focus
cargo run --example list-filter

should make it work

Keen Zach: What's the should_focus field for? The widget seems to get focus in any case.

    xliiv:

    What's the should_focus field for?

it's leftover after the concept suggested by Zarenor a few message above.. it should be removed

    xliiv:

    The widget seems to get focus in any case.

you're saying it works for you? which platofrm ? i'm using Linux, so GTK

xliiv: anyway, it doesnt work for me see gif below

druid.gif

Colin Rofls: okay yea something weird is happening

Colin Rofls: appreciate that is not super helpful

Colin Rofls: my current suspicion is something in the lens, maybe in the impl of the Map lens? going to be boarding a flight soon so won't be able to provide much more than that. :/

Colin Rofls: this does highlight that we need more & better debugging tools.

Zarenor: I've tried parsing through a bunch of rust code before. druids highly-indirected architecture makes it difficult to debug nicely in Visual Studio. I need to take a try at CLion, but I found it's project management frustrating. Currently I write rust mostly in Rider with the Rust plugin (rather than CLion). But Rider won't do unmanaged/native debugging.

Zarenor: I'm interested in working some to get 'reasonable' debugging into any of these IDEs, but I know so far most work has been in front-end, getting compiler errors, warnings, and lints exposed.

    xliiv:

    my current suspicion is something in the lens, maybe in the impl of the Map lens?

@Colin Rofls i would say no? because i removed completly lens part (and changed data type to String which is required by textbox) and still on luck

xliiv: see druid-no-lens.gif

use druid::lens::{self, LensExt};
use druid::widget::{Flex, Label, List, Padding, Scroll, TextBox, WidgetExt};
use druid::{AppLauncher, Data, Lens, LocalizedString, Widget, WindowDesc};
use std::sync::{Arc, Mutex};
use std::{thread, time};
use druid::Size;
use druid::{BoxConstraints, Env, Event, EventCtx, LayoutCtx, LifeCycle, PaintCtx, UpdateCtx};

#[derive(Clone, Data, Default, Lens, Debug)]
struct AppData {
    searched: String,
    found: Arc<Vec<String>>,
}

pub struct Focus<T: Data> {
    inner: Box<dyn Widget<T>>,
    should_focus: bool,
}

impl<T: Data> Focus<T> {
    pub fn new(inner: impl Widget<T> + 'static) -> Self {
        Self {
            inner: Box::new(inner),
            should_focus: true,
        }
    }
}

impl<T: Data> Widget<T> for Focus<T> {
    fn event(&mut self, ctx: &mut EventCtx, event: &Event, data: &mut T, env: &Env) {
        dbg!(&event);
        let edit: Option<Event> = match event {
            Event::LifeCycle(LifeCycle::WindowConnected) => {
                ctx.request_focus();
                None
            }
            _ => None,
        };
        self.inner.event(ctx, event, data, env);
    }

    fn update(&mut self, ctx: &mut UpdateCtx, old_data: Option<&T>, data: &T, env: &Env) {
        self.inner.update(ctx, old_data, data, env);
    }

    fn layout(&mut self, ctx: &mut LayoutCtx, bc: &BoxConstraints, data: &T, env: &Env) -> Size {
        bc.debug_check("Focus");
        self.inner.layout(ctx, bc, data, env)
    }

    fn paint(&mut self, paint_ctx: &mut PaintCtx, data: &T, env: &Env) {
        self.inner.paint(paint_ctx, data, env);
    }
}


fn build_widget() -> impl Widget<String> {
            Flex::row().with_child(
                Padding::new(
                    5.0,
                    Focus::new(
                        TextBox::new(),
                    )
                ),
                1.0,
            )
}

fn main() {
    dbg!("launch_gui");

    let window = WindowDesc::new(build_widget);
    let mut data = String::new();
    AppLauncher::with_window(window)
        .use_simple_logger()
        .launch(data)
        .expect("launch failed");

    dbg!("GUI killed");
}

Colin Rofls: ugh okay I think I figured it out

Colin Rofls: TextBox::new creates an Align which has a WidgetPod :rolling_eyes:

Colin Rofls: it's an interesting case, though, we hadn't really thought about this

Colin Rofls: basically i think we should cut back on embedding widgets in other widgets without it being explicit

xliiv: @Colin Rofls you're genius :)
works!
xliiv:

use druid::lens::{self, LensExt};
use druid::widget::{Flex, Label, List, Padding, Scroll, TextBox, WidgetExt};
use druid::{AppLauncher, Data, Lens, LocalizedString, Widget, WindowDesc};
use std::sync::{Arc, Mutex};
use std::{thread, time};
use druid::Size;
use druid::{BoxConstraints, Env, Event, EventCtx, LayoutCtx, LifeCycle, PaintCtx, UpdateCtx};

#[derive(Clone, Data, Default, Lens, Debug)]
struct AppData {
    searched: String,
    found: Arc<Vec<String>>,
}

pub struct Focus<T: Data> {
    inner: Box<dyn Widget<T>>,
}

impl<T: Data> Focus<T> {
    pub fn new(inner: impl Widget<T> + 'static) -> Self {
        Self {
            inner: Box::new(inner),
        }
    }
}

impl<T: Data> Widget<T> for Focus<T> {
    fn event(&mut self, ctx: &mut EventCtx, event: &Event, data: &mut T, env: &Env) {
        dbg!(&event);
// MOVED THIS LINE ABOVE THE MATCH BLOCK
        self.inner.event(ctx, event, data, env);
        let edit: Option<Event> = match event {
            Event::LifeCycle(LifeCycle::WindowConnected) => {
                ctx.request_focus();
                None
            }
            _ => None,
        };
    }

    fn update(&mut self, ctx: &mut UpdateCtx, old_data: Option<&T>, data: &T, env: &Env) {
        self.inner.update(ctx, old_data, data, env);
    }

    fn layout(&mut self, ctx: &mut LayoutCtx, bc: &BoxConstraints, data: &T, env: &Env) -> Size {
        bc.debug_check("Focus");
        self.inner.layout(ctx, bc, data, env)
    }

    fn paint(&mut self, paint_ctx: &mut PaintCtx, data: &T, env: &Env) {
        self.inner.paint(paint_ctx, data, env);
    }
}


fn build_widget() -> impl Widget<String> {
            Flex::row().with_child(
                Padding::new(
                    5.0,
                    Focus::new(
// USE RAW INSTEAD OF NEW
                        TextBox::raw(),
                    )
                ),
                1.0,
            )
}

fn main() {
    dbg!("launch_gui");

    let window = WindowDesc::new(build_widget);
    let mut data = String::new();
    AppLauncher::with_window(window)
        .use_simple_logger()
        .launch(data)
        .expect("launch failed");

    dbg!("GUI killed");
}

xliiv: added 2 comments what is changed comparing to the previous versions

xliiv: @Colin Rofls thank you!

Zarenor: I'm not entirely opposed to reducing layering here, but I feel like composability was a pretty good feature, and something desired here. I would instead propose a transparent bool on a widget pod, set at construction. When true, the pod doesn't change the ctx active and focused states for children. I feel like for align and padding (for which I don't think being active or having focus makes sense), I feel like we need something like this.

Zarenor: I think the actual 'boundary of use' for this is single-child widgets which do not themselves react to to events (maybe also don't react to data changes?). So I guess SizedBox also fits? I'm not sure anything else does.

Zarenor: To be clear: I don't want to over-engineer this, but I think the positioning/sizing/alignment widgets are pretty fundamental, and I also don't think we want to duplicate the geometric logic in the WidgetPod. So I'm just trying to keep the friction of laying out single widgets low, while not forcing these concerns to be integrated in those widgets.

Zarenor: I've thought a little more here - I think your point was that layering the focus inside the align solves the problem.
To be clear, I don't disagree. But I think it a little odd to have a widget which doesn't want/shouldn't receive focus or activation swallowing the state, and adds a layer of required thinking to which widgets are nested how, when they seem to me like they should be commutative (because the effects seem orthogonal)

Raph Levien: I haven't analyzed the issue in detail, but agree with Zarenor here. It should be possible to have a wrapper widget that is basically transparent; not doing so impacts composability. I wouldn't be surprised if the solution here is something along the lines of delegating focus down to a child widget, but I haven't figured out what the mechanism for that might be.

Colin Rofls: I think that composability is good, I just think it might be better to make it be explicit; if you want your textbox aligned, you can explicitly align it? I'm not 100% sure what the align widget is doing in textbox currently; I can't easily see the difference between TextBox::raw() and TextBox::new().

Colin Rofls: And I have in the past run into problems with WidgetPod seeming to do too much; I would be curious to see if there were a way to break it into composable pieces, like a Layout type that just had the bits of WidgetPod that were responsible for layout?

Raph Levien: We could certainly write TextBox so that it does its own alignment. The difference is subtle and has to do with context (whether it's in a flex container or not). I don't have super-strong feelings about this, but it is clear that as a general direction we want to factor out the repeated bits into composable pieces. Figuring out exactly where the boundaries are is hard (as this issue demonstrates), so I'm okay with repeating ourselves a bit until we do figure that out.

Raph Levien: I see what you're saying about WidgetPod but am skeptical that breaking it up will gain much overall system simplicity. Right now, it's one place to change things.
